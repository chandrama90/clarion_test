This project is nothing but an technical test for Clarion Technologies. 

You can run this project by following command:
### `npm install`
### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

Project requirement are as follows :
1. Create one login form with email and password
- Email and Password both are mandatory.
- Email should be a valid email format.
- Password should contain one uppercase letter.
- No need to hit server but redirect user to dashboard if he enters
Username: Clarion@clarion.com and Password: Clarion123
- Routers should be used and form validations are required.
- On dashboard page display greeting to logged in user, considering text before @ in
email is username. - “Hello {{username}}”.

2. Show the list of products and add-delete products
- Display the list of products from service/factory on dashboard (you can use dummy array
of products).
- Every product will have id, name, rate, quality (1, 2, 3), delete link.
- When you click on add product link, popup should be displayed to add product.
- You should have add & cancel options available to add product.
- When you click on add, product should get added on dashboard list as well.
- On click of cancel button popup should be closed in normal ways.
- If you click on delete link for any product should get delete from list.

OR

3. Show the list of products and edit-delete products
- Display the list of products from service/factory on dashboard (you can use dummy array
of products).
- Every product will have id, name, rate, quality (1, 2, 3), delete link.
- When you click on add product link, popup should be displayed to edit product.
- You should have update & cancel options available for edit product.
- When you click on update, product details should get updated on dashboard list as well.
- On click of cancel button popup should be closed in normal ways.
- If you click on delete link for any product should get delete from list.
