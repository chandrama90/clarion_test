import React, { useState } from 'react';
import AddProduct from './AddProduct';
import EditProduct from './EditProduct';
import List from '../utils/productList.json';

const ProductList = props => {
    const [products, setProducts] = useState(List);
    const [showPopup, setShowPopup] = useState(false);
    const [editItem, setEditItem] = useState(null);

    const productAdded = (item) => {
        const allProducts = products; 
        item['id'] = products.length + 1;
        allProducts.push(item);
        setProducts(allProducts);
    }

    const editProduct = (item) => {
        const allProducts = products;
        var objIndex = allProducts.findIndex((obj => obj.id === item.id));
        allProducts[objIndex].name = item.name;
        allProducts[objIndex].rate = item.rate;
        allProducts[objIndex].quantity = item.quantity;
        setProducts(allProducts);
    }

    const deleteItem = item => {
        const filterItems = products.filter(product => product.id !== item.id);
        setProducts(filterItems);
    }
    return (
        <div>
            {
                showPopup ?
                    <>
                    <div className="overlay"></div>
                    <AddProduct onAddProdct={productAdded} closePopup={() => setShowPopup(false)} />
                    </>
                    : null
            }
            <div className="product-list-header">
                <h5>Product List</h5>
                <button onClick={() => setShowPopup(true)}>Add Product</button>
            </div>
            {
                editItem ?
                    <>
                    <div className="overlay"></div>
                    <EditProduct onEditProdct={editProduct} item = {editItem}  closePopup={() => setEditItem(null)} />
                    </>
                    : null
            }
            <div>
                <table>
                    <thead>
                        <tr>
                            <td>No.</td>
                            <td>Name</td>
                            <td>Rate</td>
                            <td>Quantity</td>
                            <td>Action</td>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            products.map((item, index) => {
                                return (
                                    <tr key={index}>
                                        <td>{index + 1}</td>
                                        <td>{item.name}</td>
                                        <td>{item.rate}</td>
                                        <td>{item.quantity}</td>
                                        <td>
                                            <button className="btn btn-danger" onClick={() => deleteItem(item)}>Delete</button>
                                            &nbsp;
                                            <button className="btn btn-danger" onClick={() => setEditItem(item)}>Edit</button>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
            </div>

        </div>
    );
}

export default ProductList;