import React, { Component }from 'react';
import { setLocalStorageItem } from '../utils/helper';

export default class LoginForm extends Component {
    constructor() {
        super();
        this.state = {
            username: '',
            password: ''
        };
        //Clarion@clarion.com
        //Clarion123
        // this.handleSubmit = this.handleSubmit.bind(this);
        // this.handleTextChange = this.handleTextChange.bind(this);
    }

    handleTextChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        console.log("this.state.password =>",this.state.password);
        if(!this.state.username || !this.state.password){
            alert('Incorrect email and password combination...!!!');
        }
        else if(this.state.password.match(".*[A-Z].*") === null){
            alert('Password should contain at least onecapital letter...!!!');
        }
        else {
            const { username } = this.state;
            const userInfo = {
                email: username,
                name: username.split('@')[0]
            };
            setLocalStorageItem('loggedInUser', userInfo)
             this.props.history.push('/dashboard');
        }
    }

    render() {
        return (
            <div className="container">
                <div className="wrapper">
                    <form onSubmit={this.handleSubmit} className="form-signin">
                        <h2 className="form-signin-heading">Login</h2>
                        <input
                            type="email" 
                            className="form-control" 
                            name="username"
                            value={this.state.username} 
                            onChange={this.handleTextChange}                            
                            placeholder="Email Address" required
                            autoComplete="off"
                        />
                        <br />
                        <input
                            type="password" 
                            className="form-control"
                            name="password"
                            value={this.state.password} 
                            onChange={this.handleTextChange}                            
                            placeholder="Password" required
                            autoComplete="off"
                        />
                        <button className="btn btn-lg btn-primary btn-block" type="submit">Login</button>
                    </form>
                </div>
            </div>
        )
    }
}