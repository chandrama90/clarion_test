import React from 'react';
import ProductList from './ProductList';
import { getLocalStorageItem, setLocalStorageItem } from '../utils/helper';

const Dashboard = props => {
    const userInfo = getLocalStorageItem('loggedInUser');

    const logout = () => {
        console.log(props);
        setLocalStorageItem('loggedInUser', {})
        props.history.replace('/')
    }
    return (
        <div>
            <nav style={{padding: '8px 25px', background: 'skyblue'}} className="navbar navbar-light">
                <span className="navbar-brand mb-0 h1">Dashboard</span>
                <div className="user-info-wrapper">
                    <span className="greeting">Hello {userInfo.name}</span>
                    <span>|</span>
                    <button className="btn btn-primary logout" onClick={logout}>Logout</button>    
                </div>
            </nav>
            <ProductList />
        </div>
    )
}

export default Dashboard;