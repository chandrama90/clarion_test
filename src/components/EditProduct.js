import React, { useState } from 'react';

const EditProduct = ({ onEditProdct, item, closePopup }) => {
    const [name, setName] = useState(item.name);
    const [rate, setRate] = useState(item.rate);
    const [quantity, setQuantity] = useState(item.quantity);
    const [id, setId] = useState(item.id);
    
    const handleSubmit = e => {
        e.preventDefault();
        if(name && rate && quantity) {
            onEditProdct({
                name,
                rate,
                quantity,
                id
            });
            closePopup();
        }
    }

    const handleClose = e => {
        e.preventDefault();
        closePopup();
    }
    return (
        <div className="popup">
            <form onSubmit={handleSubmit}>
                <div className="form-signin">
                    <h5 className="form-signin-heading">Edit Product</h5>
                    <input
                        type="text" 
                        className="form-control" 
                        name="name"
                        value={name} 
                        onChange={(e) => setName(e.target.value)}                        
                        placeholder="Name" required
                        autoComplete="off"
                    />
                    <br />
                    <input
                        type="number" 
                        className="form-control" 
                        name="rate"
                        value={rate} 
                        onChange={(e) => setRate(e.target.value)}                        
                        placeholder="Rate" required
                        autoComplete="off"
                    />
                    <br />
                    <input
                        type="number" 
                        className="form-control" 
                        name="rate"
                        value={quantity} 
                        onChange={(e) => setQuantity(e.target.value)}                        
                        placeholder="Quantity" required
                        autoComplete="off"
                    />
                    <br />
                    <div className="btns-wrapper">
                        <button className="btn btn-primary" onClick={handleSubmit}>Edit</button> &nbsp;
                        <button className="btn btn-danger" onClick={handleClose}>Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    )
}

export default EditProduct;