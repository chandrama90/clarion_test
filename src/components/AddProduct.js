import React, { useState } from 'react';

const AddProduct = ({ onAddProdct, closePopup }) => {
    const [name, setName] = useState('');
    const [rate, setRate] = useState('');
    const [quantity, setQuantity] = useState('');
    const handleSubmit = e => {
        e.preventDefault();
        if(name && rate && quantity) {
            onAddProdct({
                name,
                rate,
                quantity
            });
            closePopup();
        }
    }

    const handleClose = e => {
        e.preventDefault();
        closePopup();
    }
    return (
        <div className="popup">
            <form onSubmit={handleSubmit}>
                <div className="form-signin">
                    <h5 className="form-signin-heading">Add Product</h5>
                    <input
                        type="text" 
                        className="form-control" 
                        name="name"
                        value={name} 
                        onChange={(e) => setName(e.target.value)}                        
                        placeholder="Name" required
                        autoComplete="off"
                    />
                    <br />
                    <input
                        type="number" 
                        className="form-control" 
                        name="rate"
                        value={rate} 
                        onChange={(e) => setRate(e.target.value)}                        
                        placeholder="Rate" required
                        autoComplete="off"
                    />
                    <br />
                    <input
                        type="number" 
                        className="form-control" 
                        name="rate"
                        value={quantity} 
                        onChange={(e) => setQuantity(e.target.value)}                        
                        placeholder="Quantity" required
                        autoComplete="off"
                    />
                    <br />
                    <div className="btns-wrapper">
                        <button className="btn btn-primary" onClick={handleSubmit}>Add</button> &nbsp;
                        <button className="btn btn-danger" onClick={handleClose}>Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    )
}

export default AddProduct;