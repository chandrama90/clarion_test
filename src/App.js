import React from 'react';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import LoginForm from './components/LoginForm';
import Dashboard from './components/Dashboard';

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

const App = props => {
  return (
    <div className="app">
      <Router>
        <Switch>
          <Route exact path="/" component={LoginForm} />
          <Route  path="/dashboard" component={Dashboard} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;